﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaColecoes
{
    class FolhaCollection
    {
        static List<Folha> Folhas = new List<Folha>();

        public static void Insert(Folha Folha)
        {
            Folhas.Add(Folha);
        }

        public static Folha Search(Folha Folha)
        {
            foreach (Folha x in Folhas)
            {
                if(x.Funcionario.Equals(Folha.Funcionario) && x.Mes == Folha.Mes && x.Ano == Folha.Ano)
                {
                    return x;
                }
            }
            return null;
        }

        public static List<Folha> GetFolhas()
        {
            return Folhas;
        }

    }
}

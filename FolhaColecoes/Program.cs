﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaColecoes
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
		    do{
			    Console.WriteLine("\n\n1 - Ler dados");
                Console.WriteLine("2 - Consultar folha");
			    Console.WriteLine("3 - Listar folha");
			    Console.WriteLine("4 - Sair");
                Console.Write("Opção: ");
			    opc = int.Parse(Console.ReadLine());
			    switch(opc){
				    case 1:
					    LerDados();
					    break;
				    case 2:
					    ConsultarFolha();					    
					    break;
                    case 3:
                        ListarFolha();
                        break;
			    }
		    }while(opc != 4);
	    }
	
	    private static void LerDados(){
		    Console.WriteLine("\n\n");
		    Folha Folha = new Folha();
            Console.Write("Nome: ");
            Folha.Funcionario = Console.ReadLine();
            Console.Write("Mês: ");
            Folha.Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            Folha.Ano = int.Parse(Console.ReadLine());
            if (FolhaCollection.Search(Folha) == null)
            {
                Console.Write("Horas trabalhadas: ");
                Folha.Horas = int.Parse(Console.ReadLine());
                Console.Write("Valor da hora: ");
                Folha.Valor = float.Parse(Console.ReadLine());
                FolhaCollection.Insert(Folha);
            }
            else
            {
                Console.WriteLine("\n\nFolha já cadastrada.");
            }
	    }
	
	    private static void ConsultarFolha(){
            Console.WriteLine("\n\n");
            Folha Folha = new Folha();
            Console.Write("Nome: ");
            Folha.Funcionario = Console.ReadLine();
            Console.Write("Mês: ");
            Folha.Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            Folha.Ano = int.Parse(Console.ReadLine());
            Folha = FolhaCollection.Search(Folha);
            if (Folha != null)
            {
                float bruto = FolhaNegocio.CalcularSalarioBruto(Folha);
                float inss = FolhaNegocio.CalcularINSS(bruto);
                float ir = FolhaNegocio.CalcularIR(bruto);
                Console.WriteLine("\n\nSalário bruto: " + bruto);
                Console.WriteLine("INSS: " + inss);
                Console.WriteLine("IR: " + ir);
                Console.WriteLine("FGTS: " + FolhaNegocio.CalcularFGTS(bruto));
                Console.WriteLine("Salário líquido: " + FolhaNegocio.CalcularSalarioLiquido(bruto, inss, ir));
            }
            else
            {
                Console.WriteLine("\n\nFolha não cadastrada.");
            }
	    }

        private static void ListarFolha()
        {
            Console.WriteLine("\n\n");
            Console.Write("Mês: ");
            int Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            int Ano = int.Parse(Console.ReadLine());
            float total = 0, ir, inss, bruto, liquido;

            foreach (Folha x in FolhaCollection.GetFolhas())
            {
                if (x.Mes == Mes && x.Ano == Ano)
                {
                    Console.WriteLine("Funcionário: " + x.Funcionario);
                    bruto = FolhaNegocio.CalcularSalarioBruto(x);
                    inss = FolhaNegocio.CalcularINSS(bruto);
                    ir = FolhaNegocio.CalcularIR(bruto);
                    liquido = FolhaNegocio.CalcularSalarioLiquido(bruto, ir, inss);
                    Console.WriteLine("Salário líquido: " + liquido);
                    total += liquido;
                    Console.WriteLine("-----------------------------------------");
                }
            }

            Console.ReadKey();
        }
    }       
    
}
